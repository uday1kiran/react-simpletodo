import React from "react";

const Stats = ({ toDoList }) => {
  let countList = toDoList.length;

  // Create two separate lists
  let countCheckedTasks = toDoList.filter((task) => task.checked).length;
  let countUncheckedTasks = toDoList.filter((task) => !task.checked).length;

  return (
    <div className="stats">
      <p className="notify">
        {countList === 0 ? (
          "You got all items clear"
        ) : (
          <>
            {" "}
            {`You have ${countList} items`}
            <br />
            {`${countUncheckedTasks} items to complete`}
            <br />
            {`${countCheckedTasks} items completed`}
          </>
        )}
      </p>
    </div>
  );
};

export default Stats;
